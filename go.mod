module gitlab.com/pay9/cosmos-client

go 1.16

replace (
	github.com/gogo/protobuf => github.com/regen-network/protobuf v1.3.3-alpha.regen.1
	github.com/keybase/go-keychain => github.com/99designs/go-keychain v0.0.0-20191008050251-8e49817e8af4
)

require (
	github.com/cosmos/cosmos-sdk v0.42.7
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/tendermint/tendermint v0.34.12 // indirect
	github.com/tyler-smith/go-bip39 v1.1.0
	google.golang.org/grpc v1.40.0
)
