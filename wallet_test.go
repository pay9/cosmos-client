package cosmosclient_test

import (
	"testing"

	"github.com/tyler-smith/go-bip39"
	cosmosclient "gitlab.com/pay9/cosmos-client"
)

func TestNewMnemonic(t *testing.T) {
	entropySeed, err := bip39.NewEntropy(256)
	if err != nil {
		t.Error(err)
	}
	mnemonic, err := bip39.NewMnemonic(entropySeed)
	if err != nil {
		t.Error(err)
	}
	t.Log(mnemonic)
}

func TestNewAccount(t *testing.T) {
	mnemonic := "parade cabbage pistol zoo lock human ranch typical short limb cake uniform snow trick believe palm reflect nose deliver range hole burger teach ugly"
	client := cosmosclient.NewClient("127.0.0.1:9090", "pay9", mnemonic)
	acc, err := client.NewAccount(0)
	if err != nil {
		t.Error(err)
	}
	t.Log(acc)
}

func TestSendCoinsTx(t *testing.T) {
	mnemonic := "parade cabbage pistol zoo lock human ranch typical short limb cake uniform snow trick believe palm reflect nose deliver range hole burger teach ugly"
	client := cosmosclient.NewClient("127.0.0.1:9090", "pay9", mnemonic)
	from := cosmosclient.Account{
		Address:       "cosmos193n9gsxx4u6rqqsfqv572kh2z6dual700qm3m8",
		AccountNumber: uint64(0), // account numbers m/44'/1'/accNums'/0/0
		Sequence:      uint64(0), // sequence numbers of the signer that is used
	}
	to := cosmosclient.Account{
		Address: "cosmos1xtvdnspmmcdzwlv4cfgtgmlv5hnmdx7zqyjcgt",
	}
	res, err := client.SendCoinsTx(from, to, "10stake")
	if err != nil {
		t.Error(err)
	}
	t.Log(res)
}
