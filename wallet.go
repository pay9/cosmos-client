package cosmosclient

import (
	"context"
	"os"

	"github.com/cosmos/cosmos-sdk/client/tx"
	"github.com/cosmos/cosmos-sdk/crypto/hd"
	"github.com/cosmos/cosmos-sdk/crypto/keyring"
	"github.com/cosmos/cosmos-sdk/crypto/keys/secp256k1"
	"github.com/cosmos/cosmos-sdk/simapp"
	sdk "github.com/cosmos/cosmos-sdk/types"
	txtypes "github.com/cosmos/cosmos-sdk/types/tx"
	"github.com/cosmos/cosmos-sdk/types/tx/signing"
	xauthsigning "github.com/cosmos/cosmos-sdk/x/auth/signing"
	banktypes "github.com/cosmos/cosmos-sdk/x/bank/types"
	"github.com/gofrs/uuid"
	"google.golang.org/grpc"
)

// Account ...
type Account struct {
	AccountNumber uint64
	UID           string
	Address       string
	Sequence      uint64
}

// Client ...
type Client struct {
	server   string // gRPC server address : default 127.0.0.1:9090
	chainID  string // chain id : default "pay9"
	mnemonic string
}

// NewClient ...
func NewClient(server, chainID, mnemonic string) *Client {
	return &Client{
		server:   server,
		chainID: chainID,
		mnemonic: mnemonic,
	}
}

// NewAccount ...
func (c *Client) NewAccount(accountNumber uint64) (*Account, error) {
	kr, err := keyring.New(sdk.KeyringServiceName(), keyring.BackendMemory, "", os.Stdin)
	if err != nil {
		return nil, err
	}
	algos, _ := kr.SupportedAlgorithms()
	algo, err := keyring.NewSigningAlgoFromString(string(hd.Secp256k1Type), algos)
	if err != nil {
		return nil, err
	}
	hdPath := hd.CreateHDPath(sdk.GetConfig().GetCoinType(), uint32(accountNumber), 0).String()
	uid, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}
	info, err := kr.NewAccount(uid.String(), c.mnemonic, "", hdPath, algo)
	if err != nil {
		return nil, err
	}
	ko, err := keyring.Bech32KeyOutput(info)
	if err != nil {
		return nil, err
	}
	account := &Account{
		AccountNumber: accountNumber,
		UID:           uid.String(),
		Address:       ko.Address,
	}
	return account, nil
}

// SendCoinsTx ...
func (c *Client) SendCoinsTx(from, to Account, coinsStr string) (*txtypes.BroadcastTxResponse, error) {
	fromAddr, err := sdk.AccAddressFromBech32(from.Address)
	if err != nil {
		return nil, err
	}
	toAddr, err := sdk.AccAddressFromBech32(to.Address)
	if err != nil {
		return nil, err
	}
	coins, err := sdk.ParseCoinsNormalized(coinsStr)
	if err != nil {
		return nil, err
	}
	encCfg := simapp.MakeTestEncodingConfig()
	txBuilder := encCfg.TxConfig.NewTxBuilder()
	msg1 := banktypes.NewMsgSend(fromAddr, toAddr, coins)
	txBuilder.SetGasLimit(200000)
	err = txBuilder.SetMsgs(msg1)
	if err != nil {
		return nil, err
	}
	privKey, err := generatePrivateKey(c.mnemonic, from.AccountNumber)
	if err != nil {
		return nil, err
	}
	priv := &secp256k1.PrivKey{Key: []byte(privKey)}
	var signsV2 []signing.SignatureV2
	signV2 := signing.SignatureV2{
		PubKey: priv.PubKey(),
		Data: &signing.SingleSignatureData{
			SignMode:  encCfg.TxConfig.SignModeHandler().DefaultMode(),
			Signature: nil,
		},
		Sequence: from.Sequence,
	}
	signsV2 = append(signsV2, signV2)
	err = txBuilder.SetSignatures(signsV2...)
	if err != nil {
		return nil, err
	}
	signsWithPriv := []signing.SignatureV2{}
	signerData := xauthsigning.SignerData{
		ChainID:       c.chainID,
		AccountNumber: from.AccountNumber,
		Sequence:      from.Sequence,
	}
	signWithPriv, err := tx.SignWithPrivKey(
		encCfg.TxConfig.SignModeHandler().DefaultMode(), signerData,
		txBuilder, priv, encCfg.TxConfig, from.Sequence)
	if err != nil {
		return nil, err
	}
	signsWithPriv = append(signsWithPriv, signWithPriv)
	err = txBuilder.SetSignatures(signsWithPriv...)
	if err != nil {
		return nil, err
	}
	txBytes, err := encCfg.TxConfig.TxEncoder()(txBuilder.GetTx())
	if err != nil {
		return nil, err
	}
	grpcConn, _ := grpc.Dial(
		c.server,
		grpc.WithInsecure(), // The SDK doesn't support any transport security mechanism.
	)
	defer grpcConn.Close()
	txClient := txtypes.NewServiceClient(grpcConn)
	grpcRes, err := txClient.BroadcastTx(
		context.Background(),
		&txtypes.BroadcastTxRequest{
			Mode:    txtypes.BroadcastMode_BROADCAST_MODE_SYNC,
			TxBytes: txBytes,
		},
	)
	if err != nil {
		return nil, err
	}
	return grpcRes, nil
}

func generatePrivateKey(mnemonic string, accountNumber uint64) ([]byte, error) {
	kr, err := keyring.New(sdk.KeyringServiceName(), keyring.BackendMemory, "", os.Stdin)
	if err != nil {
		return nil, err
	}
	algos, _ := kr.SupportedAlgorithms()
	algo, err := keyring.NewSigningAlgoFromString(string(hd.Secp256k1Type), algos)
	if err != nil {
		return nil, err
	}
	hdPath := hd.CreateHDPath(sdk.GetConfig().GetCoinType(), uint32(accountNumber), 0).String()
	derivedPriv, err := algo.Derive()(mnemonic, "", hdPath)
	if err != nil {
		return nil, err
	}
	privKey := algo.Generate()(derivedPriv)
	return []byte(privKey.String()), nil
}